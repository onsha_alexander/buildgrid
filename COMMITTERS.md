## COMMITTERS

| Name | Email |
| -------- | -------- | 
|  Carter Sande  | <carter.sande@duodecima.technology> |
|  Ed Baunton  | <edbaunton@gmail.com> |
|  Laurence Urhegyi  | <laurence.urhegyi@codethink.co.uk> |
|  Finn Ball  | <finn.ball@codethink.co.uk> |
|  Paul Sherwood  | <paul.sherwood@codethink.co.uk> |
|  James Ennis  | <james.ennis@codethink.com> |
|  Jim MacArthur  | <jim.macarthur@codethink.co.uk> |
|  Juerg Billeter  | <juerg.billeter@codethink.co.uk> |
|  Martin Blanchard  | <martin.blanchard@codethink.co.uk> |
|  Marios Hadjimichael  | <mhadjimichae@bloomberg.net> |
|  Raoul Hidalgo Charman  | <raoul.hidalgocharman@codethink.co.uk> |
|  Rohit Kothur  |  <rkothur@bloomberg.net> |
